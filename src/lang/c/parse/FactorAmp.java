package lang.c.parse;
import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class FactorAmp extends CParseRule {
    private CToken amp;
    private CParseRule number;
    public FactorAmp(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_AMPERSAND;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct = pcx.getTokenizer();
        amp = ct.getCurrentToken(pcx);
        CToken tk = ct.getNextToken(pcx);
        if (Number.isFirst(tk)) {
            number = new Number(pcx);
            number.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "&の後ろはnumberです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (number != null) {
            number.semanticCheck(pcx);
            setCType(CType.getCType(CType.T_pint));
            setConstant(true);
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; factorAmp starts");
        if (number != null) number.codeGen(pcx);
        o.println(";;; factorAmp completes");
    }
}
